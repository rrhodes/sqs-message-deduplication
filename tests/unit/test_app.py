from time import time
from unittest.mock import call

import boto3
import moto
import pytest
from botocore.exceptions import ClientError

from src import app
from src.DynamoDBClient import DynamoDBClient
from src.utils import Key, Status

LAMBDA_TIMEOUT_ENV_VAR = 'LAMBDA_TIMEOUT'


@pytest.mark.parametrize('event',
                         [None, {}, {'Records': None}, {'Records': []}],
                         ids=['no_event', 'empty_event', 'event_with_no_records', 'event_with_empty_records'])
@moto.mock_dynamodb2
def test_lambda_handler_without_records(event, mocker, monkeypatch):
    monkeypatch.setenv(LAMBDA_TIMEOUT_ENV_VAR, '3')

    mocker.patch.object(app, 'setup_dynamodb_client')
    mocker.patch.object(app, 'query_dynamodb')

    table = setup_table()

    app.setup_dynamodb_client.return_value = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)

    app.lambda_handler(event, None)

    app.query_dynamodb.assert_not_called()


@moto.mock_dynamodb2
def test_lambda_handler_with_new_record(mocker, monkeypatch):
    monkeypatch.setenv(LAMBDA_TIMEOUT_ENV_VAR, '3')

    mocker.patch.object(app, 'setup_dynamodb_client')
    mocker.patch.object(app, 'query_dynamodb')
    mocker.patch.object(app, 'update_dynamodb')

    table = setup_table()

    app.setup_dynamodb_client.return_value = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)
    app.query_dynamodb.return_value = None

    message_id = 'test_new'
    event = {
        Key.RECORDS.value: [
            {'body': '{\"' + Key.MESSAGE_ID.value + '\": \"' + message_id + '\"}'}
        ]
    }
    expected_item = {Key.MESSAGE_ID.value: message_id, Key.CONSUMPTION_COUNT.value: 0}

    app.lambda_handler(event, None)

    app.query_dynamodb.assert_called_once_with(message_id)

    assert 2 == app.update_dynamodb.call_count

    expected_update_dynamodb_calls = [call(expected_item, Status.IN_PROGRESS), call(expected_item, Status.COMPLETE)]
    app.update_dynamodb.assert_has_calls(expected_update_dynamodb_calls)


@moto.mock_dynamodb2
def test_lambda_handler_with_duplicate_in_progress_record(mocker, monkeypatch):
    monkeypatch.setenv(LAMBDA_TIMEOUT_ENV_VAR, '3')

    mocker.patch.object(app, 'setup_dynamodb_client')
    mocker.patch.object(app, 'query_dynamodb')
    mocker.patch.object(app, 'update_dynamodb')

    table = setup_table()
    message_id = 'test_in_progress'
    event = {
        Key.RECORDS.value: [
            {'body': '{\"' + Key.MESSAGE_ID.value + '\": \"' + message_id + '\"}'}
        ]
    }

    item = {
        Key.MESSAGE_ID.value: message_id,
        Key.CONSUMPTION_COUNT.value: 1,
        Key.STATUS.value: Status.IN_PROGRESS.value,
        Key.UPDATED.value: 1559990450
    }

    app.setup_dynamodb_client.return_value = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)
    app.query_dynamodb.return_value = item
    app.update_dynamodb.side_effect = app.ProcessingError

    with pytest.raises(app.ProcessingError):
        app.lambda_handler(event, None)

        app.query_dynamodb.assert_called_once_with(message_id)


@moto.mock_dynamodb2
def test_lambda_handler_with_duplicate_complete_record(mocker, monkeypatch):
    monkeypatch.setenv(LAMBDA_TIMEOUT_ENV_VAR, '3')

    mocker.patch.object(app, 'setup_dynamodb_client')
    mocker.patch.object(app, 'query_dynamodb')
    mocker.patch.object(app, 'update_dynamodb')

    table = setup_table()
    message_id = 'test_complete'
    event = {
        Key.RECORDS.value: [
            {'body': '{\"' + Key.MESSAGE_ID.value + '\": \"' + message_id + '\"}'}
        ]
    }

    item = {
        Key.MESSAGE_ID.value: message_id,
        Key.CONSUMPTION_COUNT.value: 1,
        Key.STATUS.value: Status.COMPLETE.value,
        Key.UPDATED.value: 1559990450
    }

    app.setup_dynamodb_client.return_value = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)
    app.query_dynamodb.return_value = item

    app.lambda_handler(event, None)

    app.query_dynamodb.assert_called_once_with(message_id)
    app.update_dynamodb.assert_not_called()


@moto.mock_dynamodb2
def test_setup_dynamodb_client_success(monkeypatch):
    monkeypatch.setenv('MESSAGE_TABLE_NAME', 'test-table')

    expected_table = setup_table()
    dynamodb_client = app.setup_dynamodb_client()

    assert expected_table == dynamodb_client.table


@moto.mock_dynamodb2
def test_query_dynamodb_returns_item():
    table = setup_table()

    message_id = 'test'
    expected_item = {
        Key.MESSAGE_ID.value: message_id,
        Key.CONSUMPTION_COUNT.value: 1,
        Key.STATUS.value: Status.IN_PROGRESS.value,
        Key.UPDATED.value: 1559990450
    }

    table.put_item(Item=expected_item)

    app.DYNAMODB_CLIENT = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)

    actual_item = app.query_dynamodb(message_id)

    assert expected_item == actual_item


@moto.mock_dynamodb2
def test_query_dynamodb_returns_none():
    table = setup_table()

    app.DYNAMODB_CLIENT = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)

    actual_item = app.query_dynamodb('test')

    assert actual_item is None


@moto.mock_dynamodb2
def test_query_dynamodb_exception(mocker):
    mocker.patch.object(DynamoDBClient, 'get_item')
    table = setup_table()

    DynamoDBClient.get_item.side_effect = ClientError({}, 'test_operation_name')

    app.DYNAMODB_CLIENT = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)

    with pytest.raises(ClientError):
        app.query_dynamodb('test')


@moto.mock_dynamodb2
def test_update_dynamodb_success(monkeypatch):
    monkeypatch.setenv(LAMBDA_TIMEOUT_ENV_VAR, '3')

    table = setup_table()

    message_id = 'test'
    original_updated = 1559990450
    original_item = {
        Key.MESSAGE_ID.value: message_id,
        Key.CONSUMPTION_COUNT.value: 1,
        Key.STATUS.value: Status.IN_PROGRESS.value,
        Key.UPDATED.value: original_updated
    }

    app.DYNAMODB_CLIENT = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)

    app.update_dynamodb(original_item, Status.COMPLETE)

    result = table.get_item(Key={Key.MESSAGE_ID.value: message_id}, ConsistentRead=True)

    assert result is not None
    assert Key.ITEM.value in result

    new_item = result[Key.ITEM.value]

    assert 2 == new_item[Key.CONSUMPTION_COUNT.value]
    assert Status.COMPLETE.value == new_item[Key.STATUS.value]
    assert original_updated != new_item[Key.UPDATED.value]


@moto.mock_dynamodb2
def test_update_dynamodb_in_progress_update_failure(mocker):
    mocker.patch.object(DynamoDBClient, 'put_item')
    table = setup_table()

    DynamoDBClient.put_item.side_effect = ClientError({}, 'test_operation_name')

    message_id = 'test'
    item = {
        Key.MESSAGE_ID.value: message_id,
        Key.CONSUMPTION_COUNT.value: 1,
        Key.STATUS.value: Status.IN_PROGRESS.value,
        Key.UPDATED.value: int(time())
    }

    table.put_item(Item=item)

    app.DYNAMODB_CLIENT = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)
    app.LAMBDA_TIMEOUT = 3

    with pytest.raises(app.ProcessingError):
        app.update_dynamodb(item, Status.IN_PROGRESS)

    assert 1 == DynamoDBClient.put_item.call_count


@moto.mock_dynamodb2
def test_update_dynamodb_completed_update_failure(caplog, mocker):
    mocker.patch.object(DynamoDBClient, 'put_item')
    table = setup_table()

    DynamoDBClient.put_item.side_effect = ClientError({}, 'test_operation_name')

    message_id = 'test'
    item = {
        Key.MESSAGE_ID.value: message_id,
        Key.CONSUMPTION_COUNT.value: 1,
        Key.STATUS.value: Status.COMPLETE.value,
        Key.UPDATED.value: int(time())
    }

    table.put_item(Item=item)

    app.DYNAMODB_CLIENT = DynamoDBClient(table.table_name, Key.MESSAGE_ID.value)
    app.LAMBDA_TIMEOUT = 3

    app.update_dynamodb(item, Status.COMPLETE)

    assert 1 == DynamoDBClient.put_item.call_count


def setup_table():
    dynamodb = boto3.resource('dynamodb')

    return dynamodb.create_table(
        AttributeDefinitions=[
            {
                'AttributeName': Key.MESSAGE_ID.value,
                'AttributeType': 'S'
            }
        ],
        KeySchema=[
            {
                'AttributeName': Key.MESSAGE_ID.value,
                'KeyType': 'HASH'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 5
        },
        TableName='test-table'
    )
