from enum import Enum


class Key(Enum):
    CONSUMPTION_COUNT = 'consumption_count'
    ITEM = 'Item'
    MESSAGE_ID = 'message_id'
    RECORDS = 'Records'
    STATUS = 'status'
    UPDATED = 'updated'


class Status(Enum):
    IN_PROGRESS = 'IN_PROGRESS'
    COMPLETE = 'COMPLETE'
