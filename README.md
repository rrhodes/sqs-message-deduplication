# SQS Message Deduplication
AWS Lambda and SQS stack for a Medium software engineering [blog post](https://medium.com/avmconsulting-blog/deduplicating-amazon-sqs-messages-dc114d1e6545?source=friends_link&sk=9321b9888e27e3cf2526dba8311cfc4a).

## Requirements
Install `npm` packages and `pip` dependencies.
```
npm i && pip3 install -r requirements/test.txt
```

## Unit Test
Executes unit tests for the Lambda function.
```
pytest -v tests/unit/
```

## Build
Builds the CDK stack.
```
rm -rf cdk.out && npm run build
```

## Deploy
Deploys the CDK stack.
```
rm -rf cdk.out && cdk deploy -f --require-approval=never
```

## Destroy
Destroys the CDK stack.
```
rm -rf cdk.out && cdk destroy -f --require-approval=never
```
